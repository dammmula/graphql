import './App.css';
import MessageList from "./Components/MessageList/MessageList";
import MessageInput from "./Components/MessageInput/MessageInput";

function App() {
  return (
    <div className="App">
        <MessageList />
        <MessageInput />
    </div>
  );
}

export default App;
