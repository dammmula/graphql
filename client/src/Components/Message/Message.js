import React, {useState} from "react";
import './Message.css'
import ReplyInput from "../ReplyInput/ReplyInput";
import ReplyList from "../ReplyList/ReplyList";
import uuid from "react-uuid";

const Message = (props) => {

    const [showReplyInput, toggleReplyInput] = useState(false);
    const {text, id, likes, dislikes, replies} = props;

    const onButtonClick = () => {
        toggleReplyInput(!showReplyInput);
    }


    return (
        <div className='message'>
            <div className="card">

                <div className="card-body">
                    <h5 className="card-title">{id}</h5>
                    <p className="card-text">{text}</p>
                    <div className="card-btns">
                        <button className="btn btn-primary"
                                onClick={onButtonClick}>Reply</button>
                        <div className='like-dislike'>
                            {likes}
                            {/*<i className="fas fa-thumbs-up"></i>*/}
                            <i className="far fa-thumbs-up"></i>
                            {dislikes}
                            <i className="far fa-thumbs-down"></i>
                            {/*<i className="fas fa-thumbs-down"></i>*/}
                        </div>
                    </div>
                </div>

                <ReplyList replies={replies} key={uuid()}/>

            </div>
            {showReplyInput ? <ReplyInput messageId={id}/> : null}

        </div>
    );
}

export default Message;