import React, {useState} from "react";
import {MESSAGE_QUERY, POST_REPLY_MUTATION} from "../../queries";
import {Mutation} from "react-apollo";

const ReplyInput = (props) => {
    const {messageId} = props;

    const [text, setText] = useState('');

    const _updateStoreAfterAddingReply = (store, newReply) => {
        try {
            const orderBy = 'createdAt_ASC';
            const data = store.readQuery({
                query: MESSAGE_QUERY,
                subscribes: {
                    orderBy
                }
            })
            data.messages.messageList.find(({id}) => id === messageId).replies.unshift(newReply);
            store.writeQuery({
                query: MESSAGE_QUERY,
                data
            });
        } catch (error) {
            console.warn(error);
        }
    };

    return (
      <div className='reply-input'>
          <label className="form-label">Reply</label>
          <div className="input-group mb-3">
              <input
                  type="text"
                  className="form-control"
                  value={text}
                  onChange={(e) => setText(e.target.value)} />
              <Mutation
                  mutation={POST_REPLY_MUTATION}
                  variables={{
                      text,
                      messageId
                  }}
                  update={(store, { data: {postReply}}) => {
                      _updateStoreAfterAddingReply(store, postReply);
                      setText('');
                  }}>
                  {postMutation =>
                      <button className="btn btn-outline-secondary" type="button"
                              onClick={postMutation}>Send</button>
                  }
              </Mutation>
          </div>
      </div>
    );
}

export default ReplyInput;