import React from "react";
import Message from "../Message/Message";
import {Query} from "react-apollo";
import './MessageList.css';
import {MESSAGE_QUERY, NEW_MESSAGE_SUBSCRIPTION} from "../../queries";

const MessageList = (props) => {

    const orderBy = 'createdAt_ASC';

    const _subscribeToNewMessages = subscribeToMore => {
        subscribeToMore({
            document: NEW_MESSAGE_SUBSCRIPTION,
            updateQuery: (prev, {subscriptionData}) => {
                if (!subscriptionData.data) return prev
                const {newMessage} = subscriptionData.data;
                const exists = prev.messages.messageList.find(({id}) => id === newMessage.id);
                if (exists) return prev;

                return {...prev, messages: {
                        messageList: [...prev.messages.messageList, newMessage],
                        count: prev.messages.messageList.length + 1,
                        __typename: prev.messages.__typename
                }};
            }
        })
    }

    return (

        <div className='message-list'>
            <Query query={MESSAGE_QUERY} variables={{orderBy}}>
                {
                    ({loading, error, data, subscribeToMore}) => {
                        if (loading) return <div>Loading...</div>;
                        if (error) return <div>Fetch error</div>;
                        _subscribeToNewMessages(subscribeToMore);

                        const {messages: {messageList}} = data;

                        return (
                            <div>
                                {messageList.map(message => {
                                    return <Message key={message.id} {...message}/>
                                })}
                            </div>
                        )}
                }
            </Query>
        </div>
    )
}

export default MessageList;