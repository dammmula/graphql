import React, {useState} from "react";
import {Mutation} from "react-apollo";
import {MESSAGE_QUERY, POST_MESSAGE_MUTATION} from "../../queries";

const MessageInput = (props) => {

    const [text, setText] = useState('');

    const _updateStoreAfterAddingMessage = (store, newMessage) => {
        try {
            const orderBy = 'createdAt_ASC';
            const data = store.readQuery({
                query: MESSAGE_QUERY,
                subscribes: {
                    orderBy
                }
            })
            data.messages.messageList.unshift(newMessage);
            store.writeQuery({
                query: MESSAGE_QUERY,
                data
            });
        } catch (error) {
            console.warn(error);
        }
    };

    return (
        <div className='message'>
            <label className="form-label">Message</label>
            <div className="input-group mb-3">
                <input
                    type="text"
                    className="form-control"
                    value={text}
                    onChange={(e) => setText(e.target.value)} />
                <Mutation
                    mutation={POST_MESSAGE_MUTATION}
                    variables={{text}}
                    update={(store, { data: {postMessage}}) => {
                        _updateStoreAfterAddingMessage(store, postMessage);
                        setText('');
                    }}>
                    {postMutation =>
                        <button className="btn btn-outline-secondary" type="button"
                        onClick={postMutation}>Send</button>
                    }
                </Mutation>
            </div>
        </div>
    );
}

export default MessageInput;