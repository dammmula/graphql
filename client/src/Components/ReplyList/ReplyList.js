import React from "react";
import './ReplyList.css';
import Reply from "../Reply/Reply";

const ReplyList = (props) => {
    const {replies} = props;
    return (
        <div className='reply-list'>
            {
                replies.map((reply) => <Reply {...reply} key={reply.id}/>)
            }
        </div>
    );
};

export default ReplyList;