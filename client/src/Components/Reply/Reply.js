import React from 'react';
// import

const Reply = (props) => {
    const {text, id, likes, dislikes} = props;

    return (
        <div className='reply'>
            <div className="card">
                <div className="card-body">
                    <h5 className="card-title">{id}</h5>
                    <p className="card-text">{text}</p>
                    <div className='like-dislike'>
                        {likes}
                        {/*<i className="fas fa-thumbs-up"></i>*/}
                        <i className="far fa-thumbs-up"></i>
                        {dislikes}
                        <i className="far fa-thumbs-down"></i>
                        {/*<i className="fas fa-thumbs-down"></i>*/}
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Reply;