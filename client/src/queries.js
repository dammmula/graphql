import gql from 'graphql-tag';

export const MESSAGE_QUERY = gql`
    query messageQuery($orderBy: MessageOrderByInput) {
        messages(orderBy: $orderBy) {
            count
            messageList {
                id
                text
                likes
                dislikes
                replies {
                    id
                    text
                    likes
                    dislikes
                }
            }
        }
    }
`;

export const POST_MESSAGE_MUTATION = gql`
    mutation PostMutation($text: String!) {
        postMessage(text: $text) {
            id
            text
            likes
            dislikes
            replies {
                id
                text
                likes
                dislikes
            }
        }
    }
`;

export const POST_REPLY_MUTATION = gql`
    mutation PostMutation($messageId: ID!, $text: String!) {
        postReply(text: $text messageId: $messageId) {
            id
            text
            likes
            dislikes
        }
    }
`;

export const NEW_MESSAGE_SUBSCRIPTION = gql`
    subscription {
        newMessage {
            id
            text
            likes
            dislikes
            replies {
                id
                text
                likes
                dislikes
            }
        }
    }
`;